#include <stdio.h>

// Fungsi untuk menghitung faktorial
unsigned long long faktorial(int n) {
    if (n == 0 || n == 1) {
        return 1;
    } else {
        return n * faktorial(n - 1);
    }
}

// Fungsi untuk menghitung kombinasi C(n, r)
unsigned long long kombinasi(int n, int r) {
    if (r > n) {
        return 0; // Kombinasi tidak valid jika r > n
    } else {
        return faktorial(n) / (faktorial(r) * faktorial(n - r));
    }
}

int main() {
    int tingkat;

    // Meminta input dari pengguna
    printf("Masukkan tingkat segitiga Pascal: ");
    scanf("%d", &tingkat);

    // Menghasilkan segitiga Pascal dan menampilkannya
    for (int i = 0; i < tingkat; i++) {
        // Menampilkan ruang sebelum angka di setiap baris
        for (int j = 0; j < tingkat - i - 1; j++) {
            printf(" ");
        }

        // Menampilkan angka menggunakan rumus kombinasi
        for (int j = 0; j <= i; j++) {
            printf("%llu ", kombinasi(i, j));
        }

        printf("\n");
    }

    return 0;
}
